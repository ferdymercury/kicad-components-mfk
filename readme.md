KiCad Components MFK
====================

Mithat Konar
2015-03-027

This repository contains [KiCad](http://www.kicad-pcb.org) schematic components I have developed for my own use. I am making them available to anyone that might find them useful.

I make no warranty, guarantee, promise, or assertion that they are appropriate for anything, anytime, anywhere, nor that they use best KiCad practices, nor that they won't cause kittens to eat you. But you should know that I use these in my everyday work and so far have escaped becoming feline by fodder.

License
-------
The components in these libraries are released under the LGPL3, so feel free to use them in free and/or proprietary projects. While you are free to use the components in any kind of project, bear in mind that if you make any changes to the components or make derivations, you may need to make those changes available to all.

Copyright (C) 2012-2015 Mithat Konar. All rights reserved.
